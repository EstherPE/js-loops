
// Iteracion 2

const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
	{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
	{name: 'Juan Miranda', T1: false, T2: true, T3: true},
	{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
	{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]


for (item of alumns){
    if (item.T1==false){
        if(item.T2==true && item.T3==true){
            item.isApproved=true;
        }else{
            item.isApproved=false;
        }
    }else if (item.T2==true || item.T3==true){
        item.isApproved=true;
    }else {
        item.isApproved=false;
    }
}

console.log(alumns);